package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gammazero/workerpool"
)

var (
	// We need a commandline option -max
	maxRequests = flag.Int("max", 20, "max number of concurrent requests")
)

// the type for a finished request
type result struct {
	resp *http.Response
	body []byte
	err  error
	url  string
}

// the function that processes finished requests
func process(results chan result) {
	for res := range results {
		if res.err != nil {
			log.Printf("GET %q: %v", res.url, res.err)
			continue
		}

		fmt.Printf("GET %s - %d\n", res.url, res.resp.StatusCode)
	}
}

func main() {
	flag.Parse()
	wp := workerpool.New(*maxRequests)

	results := make(chan result)
	go process(results)

	// Loop over every line in the input
	s := bufio.NewScanner(os.Stdin)
	for s.Scan() {
		// If we just used s.Text() in another gorutine
		// we would have race conditions because it will change
		// in the next iteration.
		url := s.Text()

		// Submit the request task to the worker pool.
		wp.Submit(func() {
			resp, err := http.Get(url)

			// Create our response from what we know so far
			res := result{
				url:  url,
				resp: resp,
				err:  err,
			}

			// When this function returns, send res
			// over the results channel.
			defer func() { results <- res }()

			// If our Request failed return early,
			// because we can't preform this other part.
			if err != nil {
				return
			}

			// Close the response body when this function returns
			defer resp.Body.Close()

			// A bytes.buffer will resize itself to fit more data
			b := &bytes.Buffer{}
			_, err = io.Copy(b, resp.Body)

			res.err = err        // set our res.err to be the new possible err
			res.body = b.Bytes() // give them the bytes, not the bytes.Buffer
		})
	}
	wp.StopWait() // Wait for all the tasks to complete

	// Close the results channel so the process goroutine will end.
	// this is not needed since the main function ends here, but is good practice.
	close(results)
}
